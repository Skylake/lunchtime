<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Number;

/**
 * SuppliersProductsPurchaseOrders Controller
 *
 * @property \App\Model\Table\SuppliersProductsPurchaseOrdersTable $SuppliersProductsPurchaseOrders
 *
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SuppliersProductsPurchaseOrdersController extends AppController
{
    public function isAuthorized($user)
    {
        // Run controller specific authorisation checks if
        // the parent check is returning no access was given.
        if (parent::isAuthorized($user))
            return true;

        $action = $this->request->getParam('action');
        if (in_array($action, ['add'])) {
            return true;
        }

        // All other actions require an order record id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the order record belongs to the current user.
        $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->findById($id)->first();

        return $suppliersProductsPurchaseOrder->user_id === $user['id'];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SuppliersProducts', 'PurchaseOrders', 'Users', 'SuppliersProducts.Products', 'PurchaseOrders.Suppliers']
        ];
        $suppliersProductsPurchaseOrders = $this->paginate($this->SuppliersProductsPurchaseOrders->find('all')
            ->where(['SuppliersProductsPurchaseOrders.user_id' => $this->Auth->user('id')])
            ->order(['SuppliersProductsPurchaseOrders.id' => 'DESC']));

        $this->set(compact('suppliersProductsPurchaseOrders'));
    }

    /**
     * View method
     *
     * @param string|null $id Suppliers Products Purchase Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->get($id, [
            'contain' => ['SuppliersProducts', 'PurchaseOrders', 'Users', 'SuppliersProducts.Products', 'PurchaseOrders.Suppliers']
        ]);

        $this->set('suppliersProductsPurchaseOrder', $suppliersProductsPurchaseOrder);
    }

    /**
     * Add method
     *
     * @param string|null $id Purchase Order id.
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        $purchaseOrder = $this->SuppliersProductsPurchaseOrders->PurchaseOrders->get($id, [
            'contain' => ['Suppliers']
        ]);

        $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->newEntity();
        if ($this->request->is('post')) {
            $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->patchEntity($suppliersProductsPurchaseOrder, $this->request->getData());
            
            // Set conditional values like the price, order-id and user-id.
            $suppliersProductsPurchaseOrder->purchase_order_id = $purchaseOrder->id;
            $suppliersProductsPurchaseOrder->user_id = $this->Auth->user('id');

            // receive the supplier product record to get the price of the product and calculate the total by multiplying with quantity.
            $suppliersProduct = $this->SuppliersProductsPurchaseOrders->SuppliersProducts->get($suppliersProductsPurchaseOrder->suppliers_product_id, ['key' => 'product_id']);
            $suppliersProductsPurchaseOrder->price = $suppliersProduct->price;
            
            if ($this->SuppliersProductsPurchaseOrders->save($suppliersProductsPurchaseOrder)) {
                $this->Flash->success(__('The suppliers products purchase order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suppliers products purchase order could not be saved. Please, try again.'));
        }
        
        $suppliersProducts = $this->SuppliersProductsPurchaseOrders->SuppliersProducts->find('list',
            ['keyField' => 'id', 'valueField' => ['product_details']])->where(['supplier_id' => $purchaseOrder->supplier_id])->contain(['Products']);
        // format the products list.
        $suppliersProducts->select(['id', 'product_details' => $suppliersProducts->func()->concat([
                'name' => 'literal',
                ', €',
                'price' => 'literal'
            ])
        ]);
        
        $this->set(compact('purchaseOrder', 'suppliersProductsPurchaseOrder', 'suppliersProducts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Suppliers Products Purchase Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->get($id, [
            'contain' => ['PurchaseOrders', 'PurchaseOrders.Suppliers']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->patchEntity($suppliersProductsPurchaseOrder, $this->request->getData(), [
                'accessibleFields' => [
                    'purchase_order_id' => false,    
                    'user_id' => false,
                    'paid' => false,
                ]
            ]);

            // receive the supplier product record to get the price of the product and calculate the total by multiplying with quantity.
            $suppliersProduct = $this->SuppliersProductsPurchaseOrders->SuppliersProducts->get($suppliersProductsPurchaseOrder->suppliers_product_id, ['key' => 'product_id']);
            $suppliersProductsPurchaseOrder->price = $suppliersProduct->price;
            
            if ($this->SuppliersProductsPurchaseOrders->save($suppliersProductsPurchaseOrder)) {
                $this->Flash->success(__('The suppliers products purchase order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suppliers products purchase order could not be saved. Please, try again.'));
        }
        
        $suppliersProducts = $this->SuppliersProductsPurchaseOrders->SuppliersProducts->find('list',
            ['keyField' => 'id', 'valueField' => ['product_details']])->where(['supplier_id' => $suppliersProductsPurchaseOrder->purchase_order->supplier_id])->contain(['Products']);
        // format the products list.
        $suppliersProducts->select(['id', 'product_details' => $suppliersProducts->func()->concat([
                'name' => 'literal',
                ', €',
                'price' => 'literal'
            ])
        ]);
        
        $this->set(compact('suppliersProductsPurchaseOrder', 'suppliersProducts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Suppliers Products Purchase Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->get($id);
        if ($this->SuppliersProductsPurchaseOrders->delete($suppliersProductsPurchaseOrder)) {
            $this->Flash->success(__('The suppliers products purchase order has been deleted.'));
        } else {
            $this->Flash->error(__('The suppliers products purchase order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Open method
     * 
     * @return \Cake\Http\Response|null
     */
    public function open()
    {
        $unpaidSuppliersProductsPurchaseOrders = $this->SuppliersProductsPurchaseOrders->find()
            ->select(['purchase_order_id'])
            ->distinct(['purchase_order_id'])
            ->where(['paid' => 0]);

        $unpaidPurchaseOrders = TableRegistry::getTableLocator()->get('purchase_orders')->find()
            ->where(['purchase_orders.id IN' => $unpaidSuppliersProductsPurchaseOrders, 'purchase_orders.closed' => 1])
            ->contain([
                'Suppliers',
                'Users',
                'SuppliersProductsPurchaseOrders',
                'SuppliersProductsPurchaseOrders.Users',
                'SuppliersProductsPurchaseOrders.SuppliersProducts',
                'SuppliersProductsPurchaseOrders.SuppliersProducts.Products'
            ]);
        $this->set('openOrderPayments', $unpaidPurchaseOrders);
    }

    /**
     * Pay method
     *
     * @param string|null $id Suppliers Products Purchase Order id.
     * @return \Cake\Http\Response|null Redirects to open.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function pay($id = null)
    {
        $suppliersProductsPurchaseOrder = $this->SuppliersProductsPurchaseOrders->get($id, [
            'contain' => ['PurchaseOrders', 'PurchaseOrders.Users', 'PurchaseOrders.Suppliers', 'Users']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $paymentMethod = $this->request->getData()['payment_method'];
            if ($paymentMethod == 'account_balance' || $paymentMethod == 'auth_balance') {
                $paymentAccount = $suppliersProductsPurchaseOrder->user;
                if ($paymentMethod == 'auth_balance') {
                    $paymentAccount = TableRegistry::getTableLocator()->get('users')->get($this->Auth->user('id'));
                }

                if (bccomp($paymentAccount->balance, $suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price) != -1) {
                    $suppliersProductsPurchaseOrder->paid = 1;
                    $paymentAccount->balance -= $suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price;

                    if ($this->SuppliersProductsPurchaseOrders->save($suppliersProductsPurchaseOrder) && TableRegistry::getTableLocator()->get('users')->save($paymentAccount)) {
                        $this->Flash->success(__('The suppliers products purchase order has been paid using account balance.'));
                    } else {
                        $this->Flash->error(__('The suppliers products purchase order could not be paid. Please, try again.'));
                    }
                } else {
                    $this->Flash->error(__('The suppliers product purchase order could not be paid. Unsufficient balance found on the given account.'));
                }
            } else if ($paymentMethod == 'account_iban' || $paymentMethod == 'auth_iban' || $paymentMethod == 'purchaser') { 
                $suppliersProductsPurchaseOrder->paid = 1;
                if ($this->SuppliersProductsPurchaseOrders->save($suppliersProductsPurchaseOrder)) {
                    if ($paymentMethod == 'purchaser') {
                        $this->Flash->success(__('The suppliers products purchase order has marked as paid.'));
                    } else {
                        $this->Flash->success(__('The suppliers products purchase order has been paid using bank transfer.'));
                    }
                } else {
                    $this->Flash->error(__('The suppliers products purchase order could not be paid. Please, try again.'));
                }
            }
            return $this->redirect(['action' => 'open']);
        }
        
        $userPaymentMethods = [];
        if (bccomp($suppliersProductsPurchaseOrder->user->balance, $suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price) != -1) {
            $userPaymentMethods = array_merge($userPaymentMethods, ['account_balance' => __(sprintf('Use account balance (%s %s, %s remaining)', $suppliersProductsPurchaseOrder->user->first_name, $suppliersProductsPurchaseOrder->user->last_name, Number::currency($suppliersProductsPurchaseOrder->user->balance, 'EUR')))]);
        }

        if ($suppliersProductsPurchaseOrder->user->iban != '' && $suppliersProductsPurchaseOrder->purchase_order->user->iban != '') {
            $userPaymentMethods = array_merge($userPaymentMethods, ['account_iban' => __(sprintf('Use bank transfer from `%s` to `%s`, owned by %s %s.', $suppliersProductsPurchaseOrder->user->iban, $suppliersProductsPurchaseOrder->purchase_order->user->iban, $suppliersProductsPurchaseOrder->purchase_order->user->first_name, $suppliersProductsPurchaseOrder->purchase_order->user->last_name))]);
        }

        if ($suppliersProductsPurchaseOrder->user->id != $this->Auth->user('id')) {
            $userPaymentMethods = array_merge($userPaymentMethods, ['auth_balance' => __(sprintf('Use current account balance (%s %s, %s remaining)', $this->Auth->user('first_name'), $this->Auth->user('last_name'), Number::currency($this->Auth->user('balance'), 'EUR')))]);

            if ($this->Auth->user('iban') != '' && $suppliersProductsPurchaseOrder->purchase_order->user->iban != '') {
                $userPaymentMethods = array_merge($userPaymentMethods, ['auth_iban' => __(sprintf('Use bank transfer from `%s` to `%s`, owned by %s %s.', $this->Auth->user('iban'), $suppliersProductsPurchaseOrder->purchase_order->user->iban, $suppliersProductsPurchaseOrder->purchase_order->user->first_name, $suppliersProductsPurchaseOrder->purchase_order->user->last_name))]);                
            }
        }

        if ($this->Auth->user('id') == $suppliersProductsPurchaseOrder->purchase_order->user->id) {
            $userPaymentMethods = array_merge($userPaymentMethods, ['purchaser' => __(sprintf('As purchase order owner, mark order line as paid.'))]);
        }


        if (!$userPaymentMethods) {
            $this->Flash->error(__('The owner of the given suppliers products purchase order doesn\'t have any payment method configured!'));
            return $this->redirect(['action' => 'open']);
        }
        
        $this->set(compact('suppliersProductsPurchaseOrder', 'userPaymentMethods'));
    }
}
