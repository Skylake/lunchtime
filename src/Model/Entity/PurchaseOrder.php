<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseOrder Entity
 *
 * @property int $id
 * @property int $supplier_id
 * @property int $user_id
 * @property bool $delivery
 * @property \Cake\I18n\FrozenTime $delivery_moment
 * @property float $delivery_cost
 * @property float $total_amount
 * @property string|null $comment
 * @property bool $closed
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Supplier $supplier
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\SuppliersProduct[] $suppliers_products
 */
class PurchaseOrder extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'supplier_id' => true,
        'user_id' => true,
        'delivery' => true,
        'delivery_moment' => true,
        'delivery_cost' => true,
        'total_amount' => true,
        'comment' => true,
        'closed' => true,
        'created' => true,
        'modified' => true,
        'supplier' => true,
        'user' => true,
        'suppliers_products' => true
    ];
}
