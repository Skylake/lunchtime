<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\I18n\Number;

/**
 * PurchaseOrders Controller
 *
 * @property \App\Model\Table\PurchaseOrdersTable $PurchaseOrders
 *
 * @method \App\Model\Entity\PurchaseOrder[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PurchaseOrdersController extends AppController
{
    public function isAuthorized($user)
    {
        // Run controller specific authorisation checks if
        // the parent check is returning no access was given.
        if (parent::isAuthorized($user))
            return true;

        // Allow order managers to manage their own orders.
        $role = TableRegistry::getTableLocator()->get('roles')->findByName('Besteller')->first();

        // Access for non-managers will be denied by default.
        if (!$role->id === $user['role_id'])
            return false;

        $action = $this->request->getParam('action');
        if (in_array($action, ['add'])) {
            return true;
        }

        // All other actions require an order id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the order record belongs to the current user.
        $purchaseOrder = $this->PurchaseOrders->findById($id)->first();

        return $purchaseOrder->user_id === $user['id'];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Suppliers', 'Users']
        ];
        $purchaseOrders = $this->paginate($this->PurchaseOrders->find('all')->order(['PurchaseOrders.created' => 'DESC']));

        $this->set(compact('purchaseOrders'));
    }

    /**
     * Open method
     *
     * @return \Cake\Http\Response|null
     */
    public function open()
    {
        $this->paginate = [
            'contain' => ['Suppliers', 'Users']
        ];
        $purchaseOrders = $this->paginate($this->PurchaseOrders->find('all')->where(['closed' => 0])->order(['PurchaseOrders.created' => 'DESC']));

        $this->set(compact('purchaseOrders'));
        $this->render('index');
    }

    /**
     * View method
     *
     * @param string|null $id Purchase Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $purchaseOrder = $this->PurchaseOrders->get($id, [
            'contain' => [
                'Suppliers',
                'Users',
                'SuppliersProductsPurchaseOrders',
                'SuppliersProductsPurchaseOrders.Users',
                'SuppliersProductsPurchaseOrders.SuppliersProducts',
                'SuppliersProductsPurchaseOrders.SuppliersProducts.Products'
            ]
        ]);

        $this->set('purchaseOrder', $purchaseOrder);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $purchaseOrder = $this->PurchaseOrders->newEntity();
        if ($this->request->is('post')) {
            $purchaseOrder = $this->PurchaseOrders->patchEntity($purchaseOrder, $this->request->getData());

            // Assign the current user-id as purchase order owner.
            $purchaseOrder->user_id = $this->Auth->user('id');

            if ($this->PurchaseOrders->save($purchaseOrder)) {
                $this->Flash->success(__('The purchase order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The purchase order could not be saved. Please, try again.'));
        }
        $suppliers = $this->PurchaseOrders->Suppliers->find('list', ['order' => ['suppliers.name' => 'ASC']]);

        $this->set(compact('purchaseOrder', 'suppliers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Purchase Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $purchaseOrder = $this->PurchaseOrders->get($id, [
            'contain' => ['SuppliersProducts']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $purchaseOrder = $this->PurchaseOrders->patchEntity($purchaseOrder, $this->request->getData(), [
                'accessibleFields' => [
                    'supplier_id' => false,
                    'user_id' => false,
                ]
            ]);
            if ($this->PurchaseOrders->save($purchaseOrder)) {
                $this->Flash->success(__('The purchase order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The purchase order could not be saved. Please, try again.'));
        }

        $this->set(compact('purchaseOrder'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Purchase Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $purchaseOrder = $this->PurchaseOrders->get($id);
        if ($this->PurchaseOrders->delete($purchaseOrder)) {
            $this->Flash->success(__('The purchase order has been deleted.'));
        } else {
            $this->Flash->error(__('The purchase order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Close method
     *
     * @param string|null $id Purchase Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function close($id = null)
    {
        $this->request->allowMethod(['post']);
        $purchaseOrder = $this->PurchaseOrders->get($id);
        $purchaseOrder->closed = 1;

        if ($this->PurchaseOrders->save($purchaseOrder)) {
            $this->Flash->success(__('The purchase order has been closed.'));
        } else {
            $this->Flash->error(__('The purchase order could not be closed. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Send method
     *
     * @param string|null $id Purchase Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function send($id = null)
    {
        $purchaseOrder = $this->PurchaseOrders->get($id, [
            'contain' => [
                'Suppliers',
                'Users',
                'SuppliersProductsPurchaseOrders',
                'SuppliersProductsPurchaseOrders.Users',
                'SuppliersProductsPurchaseOrders.SuppliersProducts',
                'SuppliersProductsPurchaseOrders.SuppliersProducts.Products'
            ]
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->request->getData()['email'] == '') {
                $this->Flash->error(__('A valid purchase order recipient e-mail address is required.'));
                return $this->redirect(['action' => 'send']);
            }
            $purchaseOrderMessage = '<table>';
            foreach ($purchaseOrder->suppliers_products_purchase_orders as $suppliersProductsPurchaseOrder) {
                if ($purchaseOrderMessage == '<table>') {
                    $purchaseOrderMessage .= '<tr><td width=150px><b>Name</b></td><td width=200px><b>Product</b></td><td width=200px><b>Comment</b></td><td width=100px><b>Quantity</b></td><td width=100px><b>Price</b></td><td width=150px><b>Total</b></td></tr>';
                }

                $purchaseOrderMessage .= PHP_EOL . '<tr><td>'.$suppliersProductsPurchaseOrder->user->first_name .' '. $suppliersProductsPurchaseOrder->user->last_name.'</td>';
                $purchaseOrderMessage .= '<td>'.$suppliersProductsPurchaseOrder->suppliers_product->product->name.'</td>';
                $purchaseOrderMessage .= '<td>'.$suppliersProductsPurchaseOrder->comment.'</td>';
                $purchaseOrderMessage .= '<td>'.$suppliersProductsPurchaseOrder->quantity.'</td>';
                $purchaseOrderMessage .= '<td>'. Number::currency($suppliersProductsPurchaseOrder->suppliers_product->price, 'EUR') .'</td>';
                $purchaseOrderMessage .= '<td>'. Number::currency($suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->suppliers_product->price, 'EUR') .'</td></tr>';
            }
            $purchaseOrderMessage .= '</table>';

            $purchaseOrderMessage .= '<br><b>Ordered by:</b> '. $purchaseOrder->user->first_name .' '. $purchaseOrder->user->last_name .'<br>';
            $purchaseOrderMessage .= '<b>Comment:</b> '. $purchaseOrder->comment .'<br>';
            if ($purchaseOrder->delivery) {
                $purchaseOrderMessage .= '<b>Delivery:</b> Yes<br>';
                $purchaseOrderMessage .= '<b>Delivery cost:</b> '. Number::currency($purchaseOrder->delivery_cost, 'EUR') .'<br>';
                $purchaseOrderMessage .= '<b>Delivery date/time:</b> '. $purchaseOrder->delivery_moment .'<br>';
            }
            $purchaseOrderMessage .= '<br><b>Total Amount:</b> '. Number::currency($purchaseOrder->total_amount, 'EUR');
            
            $email = new Email();
            $email->emailFormat('html')
                ->setFrom([$this->Auth->user('email') => $this->Auth->user('first_name').' '.$this->Auth->user('last_name')])
                ->setTo($this->request->getData()['email'])
                ->setSubject('Lunch order')
                ->send($purchaseOrderMessage);
            
            $this->Flash->success(__('The purchase order has been sent.'));
            return $this->redirect(['action' => 'index']);
        }
        
        $this->set(compact('purchaseOrder'));
    }
}
