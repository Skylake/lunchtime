<ul class="side-nav">
    <li class="heading"><?= __('Producten') ?></li>
    <li><?= $this->Html->link(__('Overzicht'), ['controller' => 'Products', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('Toevoegen'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    <?php if (isset($product->id)): ?>
        <li><?= $this->Html->link(__('Aanpassen'), ['controller' => 'Products', 'action' => 'edit', $product->id]) ?></li>
        <li><?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'Products', 'action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?></li>
    <?php endif; ?>
</ul>