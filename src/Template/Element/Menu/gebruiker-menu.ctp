<ul class="side-nav">
    <li class="heading"><?= __('Gebruikers') ?></li>
    <li><?= $this->Html->link(__('Overzicht'), ['controller' => 'Users', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('Toevoegen'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    <?php if (isset($user->id)): ?>
        <li><?= $this->Html->link(__('Aanpassen'), ['controller' => 'Users', 'action' => 'edit', $user->id]) ?></li>
        <!-- li><--?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'Users', 'action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?></li-->
    <?php endif; ?>
    <li class="heading"><?= __('Groepen') ?></li>
    <li><?= $this->Html->link(__('Overzicht'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('Toevoegen'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
    <?php if (isset($role->id)): ?>
        <li><?= $this->Html->link(__('Aanpassen'), ['controller' => 'Roles', 'action' => 'edit', $role->id]) ?></li>
        <li><?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'Users', 'action' => 'delete', $role->id], ['confirm' => __('Are you sure you want to delete # {0}?', $role->id)]) ?></li>
    <?php endif; ?>
</ul>