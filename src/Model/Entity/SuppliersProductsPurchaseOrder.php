<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SuppliersProductsPurchaseOrder Entity
 *
 * @property int $id
 * @property int $suppliers_product_id
 * @property int $purchase_order_id
 * @property int $user_id
 * @property int $quantity
 * @property float $price
 * @property string|null $comment
 * @property bool $paid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\SuppliersProduct $suppliers_product
 * @property \App\Model\Entity\PurchaseOrder $purchase_order
 * @property \App\Model\Entity\User $user
 */
class SuppliersProductsPurchaseOrder extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'suppliers_product_id' => true,
        'purchase_order_id' => true,
        'user_id' => true,
        'quantity' => true,
        'price' => true,
        'comment' => true,
        'paid' => true,
        'created' => true,
        'modified' => true,
        'suppliers_product' => true,
        'purchase_order' => true,
        'user' => true
    ];
}
