<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * PurchaseOrders Model
 *
 * @property \App\Model\Table\SuppliersTable&\Cake\ORM\Association\BelongsTo $Suppliers
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\SuppliersProductsTable&\Cake\ORM\Association\BelongsToMany $SuppliersProducts
 *
 * @method \App\Model\Entity\PurchaseOrder get($primaryKey, $options = [])
 * @method \App\Model\Entity\PurchaseOrder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PurchaseOrder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PurchaseOrder|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PurchaseOrder saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PurchaseOrder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PurchaseOrder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PurchaseOrder findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PurchaseOrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('purchase_orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Suppliers', [
            'foreignKey' => 'supplier_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('SuppliersProducts', [
            'foreignKey' => 'purchase_order_id',
            'targetForeignKey' => 'suppliers_product_id',
            'joinTable' => 'suppliers_products_purchase_orders'
        ]);

        $this->hasMany('SuppliersProductsPurchaseOrders', [
            'foreignKey' => 'purchase_order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('delivery')
            ->notEmptyString('delivery');

        $validator
            ->dateTime('delivery_moment')
            ->requirePresence('delivery_moment', 'create')
            ->notEmptyDateTime('delivery_moment');

        $validator
            ->decimal('delivery_cost')
            ->notEmptyString('delivery_cost');

        $validator
            ->decimal('total_amount')
            ->notEmptyString('total_amount');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->allowEmptyString('comment');

        $validator
            ->boolean('closed')
            ->notEmptyString('closed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['supplier_id'], 'Suppliers'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * beforeSave event to set delivery cost and total amount before saving.
     * This action is also called by SuppliersProductsPurchaseOrdersTable.php.
     */
    public function beforeSave(\Cake\Event\Event $event, $entity, $options)
    {
        $purchaseOrderSupplier = TableRegistry::getTableLocator()->get('Suppliers')->get($entity->supplier_id);
        $suppliersProductsPurchaseOrders = TableRegistry::getTableLocator()->get('SuppliersProductsPurchaseOrders')
            ->find('all')->where(['purchase_order_id' => $entity->id]);
        
        // initial values to zero.
        $entity->delivery_cost = 0;
        $entity->total_amount  = 0;

        // Add up all purchase order record totals.
        foreach($suppliersProductsPurchaseOrders as $suppliersProductsPurchaseOrder) {
            $entity->total_amount += $suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price;
        }

        // Eventually add the possible delivery costs.
        if ($entity->delivery && $purchaseOrderSupplier->delivery) {
            // if ($entity->total_amount >= $purchaseOrderSupplier->delivery_cost_minimum) {
                // If there is no delivery_cost_until defined, or the total amount is smaller, add the delivery cost. 
                if ($purchaseOrderSupplier->delivery_cost_until === 0.0 || bccomp($entity->total_amount, $purchaseOrderSupplier->delivery_cost_until) == -1) {
                    $entity->delivery_cost = $purchaseOrderSupplier->delivery_cost;
                    $entity->total_amount += $entity->delivery_cost;
                }
            //} 
        }
    }
}
