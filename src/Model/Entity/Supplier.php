<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supplier Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $website
 * @property string|null $phone
 * @property string|null $email
 * @property string $address
 * @property string $postalcode
 * @property string $city
 * @property bool $delivery
 * @property float $delivery_cost
 * @property float $delivery_cost_until
 * @property float $delivery_cost_minimum
 * @property string|resource|null $attachment
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\PurchaseOrder[] $purchase_orders
 * @property \App\Model\Entity\Product[] $products
 */
class Supplier extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'website' => true,
        'phone' => true,
        'email' => true,
        'address' => true,
        'postalcode' => true,
        'city' => true,
        'delivery' => true,
        'delivery_cost' => true,
        'delivery_cost_until' => true,
        'delivery_cost_minimum' => true,
        'attachment' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'purchase_orders' => true,
        'products' => true
    ];
}
