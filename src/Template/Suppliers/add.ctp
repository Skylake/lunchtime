<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $supplier
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/leverancier-menu') ?>
</nav>
<div class="suppliers form large-10 medium-9 columns content">
    <?= $this->Form->create($supplier) ?>
    <fieldset>
        <legend><?= __('Add Supplier') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('website');
            echo $this->Form->control('phone');
            echo $this->Form->control('email');
            echo $this->Form->control('address');
            echo $this->Form->control('postalcode');
            echo $this->Form->control('city');
            echo $this->Form->control('delivery');
            echo $this->Form->control('delivery_cost');
            echo $this->Form->control('delivery_cost_until');
            echo $this->Form->control('delivery_cost_minimum');
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
