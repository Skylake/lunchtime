<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProduct $suppliersProduct
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/assortiment-menu') ?>
</nav>
<div class="suppliersProducts view large-10 medium-9 columns content">
    <h3><?= h($suppliersProduct->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Supplier') ?></th>
            <td><?= $suppliersProduct->has('supplier') ? $this->Html->link($suppliersProduct->supplier->name, ['controller' => 'Suppliers', 'action' => 'view', $suppliersProduct->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $suppliersProduct->has('product') ? $this->Html->link($suppliersProduct->product->name, ['controller' => 'Products', 'action' => 'view', $suppliersProduct->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($suppliersProduct->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->currency($suppliersProduct->price, 'EUR') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($suppliersProduct->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($suppliersProduct->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Purchase Orders') ?></h4>
        <?php if (!empty($suppliersProduct->purchase_orders)): ?>
        <table>
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Supplier Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Delivery') ?></th>
                <th scope="col"><?= __('Delivery Moment') ?></th>
                <th scope="col"><?= __('Delivery Cost') ?></th>
                <th scope="col"><?= __('Total Amount') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Closed') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($suppliersProduct->purchase_orders as $purchaseOrders): ?>
            <tr>
                <td><?= h($purchaseOrders->id) ?></td>
                <td><?= h($purchaseOrders->supplier_id) ?></td>
                <td><?= h($purchaseOrders->user_id) ?></td>
                <td><?= $purchaseOrders->delivery ? __('Yes') : __('No') ?></td>
                <td><?= h($purchaseOrders->delivery_moment) ?></td>
                <td><?= $this->Number->currency($purchaseOrders->delivery_cost, 'EUR') ?></td>
                <td><?= $this->Number->currency($purchaseOrders->total_amount, 'EUR') ?></td>
                <td><?= h($purchaseOrders->comment) ?></td>
                <td><?= $purchaseOrders->closed ? __('Yes') : __('No') ?></td>
                <td><?= h($purchaseOrders->created) ?></td>
                <td><?= h($purchaseOrders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PurchaseOrders', 'action' => 'edit', $purchaseOrders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PurchaseOrders', 'action' => 'delete', $purchaseOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
