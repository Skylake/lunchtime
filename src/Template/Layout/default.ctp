<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Lunchtime: the rapid lunch provider.';
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar><!-- role="navigation" -->
        <ul class="title-area large-2 medium-3 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>

        <div class="top-bar-section">
            <ul class="left">
                <li><?= $this->Html->link(__('Bestellingen'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('Leveranciers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('Assortiment'), ['controller' => 'SuppliersProducts', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('Producten'), ['controller' => 'Products', 'action' => 'index']) ?></li>
                <li><?= $this->Html->link(__('Gebruikers'), ['controller' => 'Users', 'action' => 'index']) ?></li>
            </ul>
        </div>

        <div class="top-bar-section">
            <ul class="right">
                <!-- li><a target="_blank" href="https://book.cakephp.org/3.0/">Documentation</a></li>
                <li><a target="_blank" href="https://api.cakephp.org/3.0/">API</a></li -->

                <?php if ($this->request->session()->read('Auth.User.id')): ?>
                    <li><?= $this->Html->link(__('Ingelogd als: '.$this->request->session()->read('Auth.User.first_name')), ['controller' => 'Users', 'action' => 'view', $this->request->session()->read('Auth.User.id')]) ?></li>
                    <li><?= $this->Html->link(__('Afmelden'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
                <?php else: ?>
                    <li><?= $this->Html->link(__('Registreren'), ['controller' => 'Users', 'action' => 'register']) ?></li>
                    <li><?= $this->Html->link(__('Aanmelden'), ['controller' => 'Users', 'action' => 'login']) ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
