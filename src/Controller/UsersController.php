<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        // Always allow allow the registration and logout page.
        $this->Auth->allow(['register', 'logout']);
    }

    public function isAuthorized($user)
    {
        // Run controller specific authorisation checks if
        // the parent check is returning no access was given.
        if (parent::isAuthorized($user))
            return true;

        $action = $this->request->getParam('action');
        if (in_array($action, ['add','register', 'logout'])) {
            return true;
        }

        // All other actions require an user id.
        $id = $this->request->getParam('pass.0');
        if (!$id) {
            return false;
        }

        // Check that the account belongs to the current user.
        $account = $this->Users->findById($id)->first();

        return $account->id === $user['id'];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Roles']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * Login method
     * 
     *  @return \Cake\Http\Response|null
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            // Only accept active users.
            if ($user && $user['active']) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }
    
    /**
     * Logout method
     *
     * @return \Cake\Http\Response|null
     **/
    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'PurchaseOrders', 'PurchaseOrders.Suppliers', 'PurchaseOrders.Users', 'SuppliersProductsPurchaseOrders', 'SuppliersProductsPurchaseOrders.Users', 'SuppliersProductsPurchaseOrders.SuppliersProducts', 'SuppliersProductsPurchaseOrders.SuppliersProducts.Products']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
    }

    /**
     * Register method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            if (isset($this->request->getData()['GDPR']) && $this->request->getData()['GDPR'] == 1) {
                $user = $this->Users->patchEntity($user, $this->request->getData(), [
                    // Disable modification of certain admin-only fields.
                    'accessibleFields' => [
                        'balance' => false,
                        'role_id' => false,
                        'active'  => false,
                    ]
                ]);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The user has been saved.'));

                    return $this->redirect(['action' => 'login']);
                }
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
            else {
                $this->Flash->error(__('Please agree terms to register your account.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        // check if the authenticated user is authorized to modify balance, role and active.
        $isAdmin = (AppController::isAuthorized($this->Auth->user()));
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($isAdmin) {
                $user = $this->Users->patchEntity($user, $this->request->getData());
            } else {
                $user = $this->Users->patchEntity($user, $this->request->getData(), [
                    // Disable modification of certain admin-only fields.
                    'accessibleFields' => [
                        'balance' => false,
                        'role_id' => false,
                        'active'  => false,
                    ]
                ]);
            }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'isAdmin'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
