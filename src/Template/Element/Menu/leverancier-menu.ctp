<ul class="side-nav">
    <li class="heading"><?= __('Leveranciers') ?></li>
    <li><?= $this->Html->link(__('Overzicht'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('Toevoegen'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
    <?php if (isset($supplier->id)): ?>
        <li><?= $this->Html->link(__('Aanpassen'), ['controller' => 'Suppliers', 'action' => 'edit', $supplier->id]) ?></li>
        <li><?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'Suppliers', 'action' => 'delete', $supplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $supplier->id)]) ?></li>
    <?php endif; ?>
</ul>