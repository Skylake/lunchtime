<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SuppliersProducts Controller
 *
 * @property \App\Model\Table\SuppliersProductsTable $SuppliersProducts
 *
 * @method \App\Model\Entity\SuppliersProduct[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SuppliersProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Suppliers', 'Products']
        ];
        $suppliersProducts = $this->paginate($this->SuppliersProducts->find('all', ['order' => ['suppliers.name' => 'ASC', 'products.name' => 'ASC']]));

        $this->set(compact('suppliersProducts'));
    }

    /**
     * View method
     *
     * @param string|null $id Suppliers Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $suppliersProduct = $this->SuppliersProducts->get($id, [
            'contain' => ['Suppliers', 'Products', 'PurchaseOrders']
        ]);

        $this->set('suppliersProduct', $suppliersProduct);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $suppliersProduct = $this->SuppliersProducts->newEntity();
        if ($this->request->is('post')) {
            $suppliersProduct = $this->SuppliersProducts->patchEntity($suppliersProduct, $this->request->getData());
            if ($this->SuppliersProducts->save($suppliersProduct)) {
                $this->Flash->success(__('The suppliers product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suppliers product could not be saved. Please, try again.'));
        }
        $suppliers = $this->SuppliersProducts->Suppliers->find('list', ['limit' => 200]);
        $products = $this->SuppliersProducts->Products->find('list', ['limit' => 200]);
        $this->set(compact('suppliersProduct', 'suppliers', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Suppliers Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $suppliersProduct = $this->SuppliersProducts->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $suppliersProduct = $this->SuppliersProducts->patchEntity($suppliersProduct, $this->request->getData());
            if ($this->SuppliersProducts->save($suppliersProduct)) {
                $this->Flash->success(__('The suppliers product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suppliers product could not be saved. Please, try again.'));
        }
        $suppliers = $this->SuppliersProducts->Suppliers->find('list', ['limit' => 200]);
        $products = $this->SuppliersProducts->Products->find('list', ['limit' => 200]);
        $this->set(compact('suppliersProduct', 'suppliers', 'products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Suppliers Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $suppliersProduct = $this->SuppliersProducts->get($id);
        if ($this->SuppliersProducts->delete($suppliersProduct)) {
            $this->Flash->success(__('The suppliers product has been deleted.'));
        } else {
            $this->Flash->error(__('The suppliers product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
