<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-12 medium-12 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Register User') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('phone');
            echo $this->Form->control('email');
            echo $this->Form->control('iban');
            echo $this->Form->control('password');

            echo $this->Form->checkbox('GDPR', ['value' => 1]);
            echo $this->Form->label('GDPR', 'Let op, bij het registreren van een account ga je akkoord met dat je persoonlijke gegevens zichtbaar zijn voor medegebruikers.');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
