<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProduct $suppliersProduct
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/assortiment-menu') ?>
</nav>
<div class="suppliersProducts form large-10 medium-9 columns content">
    <?= $this->Form->create($suppliersProduct) ?>
    <fieldset>
        <legend><?= __('Edit Suppliers Product') ?></legend>
        <?php
            echo $this->Form->control('supplier_id', ['options' => $suppliers]);
            echo $this->Form->control('product_id', ['options' => $products]);
            echo $this->Form->control('price');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
