<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProduct[]|\Cake\Collection\CollectionInterface $suppliersProducts
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/assortiment-menu') ?>
</nav>
<div class="suppliersProducts index large-10 medium-9 columns content">
    <h3><?= __('Suppliers Products') ?></h3>
    <table>
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($suppliersProducts as $suppliersProduct): ?>
            <tr>
                <td><?= $this->Number->format($suppliersProduct->id) ?></td>
                <td><?= $suppliersProduct->has('supplier') ? $this->Html->link($suppliersProduct->supplier->name, ['controller' => 'Suppliers', 'action' => 'view', $suppliersProduct->supplier->id]) : '' ?></td>
                <td><?= $suppliersProduct->has('product') ? $this->Html->link($suppliersProduct->product->name, ['controller' => 'Products', 'action' => 'view', $suppliersProduct->product->id]) : '' ?></td>
                <td><?= $this->Number->currency($suppliersProduct->price, 'EUR') ?></td>
                <td><?= h($suppliersProduct->created) ?></td>
                <td><?= h($suppliersProduct->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $suppliersProduct->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $suppliersProduct->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $suppliersProduct->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliersProduct->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
