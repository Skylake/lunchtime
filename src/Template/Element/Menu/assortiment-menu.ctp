<ul class="side-nav">
    <li class="heading"><?= __('Assortiment') ?></li>
    <li><?= $this->Html->link(__('Overzicht'), ['controller' => 'SuppliersProducts', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('Toevoegen'), ['controller' => 'SuppliersProducts', 'action' => 'add']) ?></li>
    <?php if (isset($suppliersProduct->id)): ?>
        <li><?= $this->Html->link(__('Aanpassen'), ['controller' => 'SuppliersProducts', 'action' => 'edit', $suppliersProduct->id]) ?></li>
        <li><?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'SuppliersProducts', 'action' => 'delete', $suppliersProduct->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliersProduct->id)]) ?></li>
    <?php endif; ?>
</ul>