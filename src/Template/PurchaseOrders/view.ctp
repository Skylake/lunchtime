<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PurchaseOrder $purchaseOrder
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="purchaseOrders view large-10 medium-9 columns content">
    <h3><?= h($purchaseOrder->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Supplier') ?></th>
            <td><?= $purchaseOrder->has('supplier') ? $this->Html->link($purchaseOrder->supplier->name, ['controller' => 'Suppliers', 'action' => 'view', $purchaseOrder->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $purchaseOrder->has('user') ? $this->Html->link($purchaseOrder->user->first_name .' '. $purchaseOrder->user->last_name, ['controller' => 'Users', 'action' => 'view', $purchaseOrder->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comment') ?></th>
            <td><?= h($purchaseOrder->comment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($purchaseOrder->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery Cost') ?></th>
            <td><?= $this->Number->currency($purchaseOrder->delivery_cost, 'EUR') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Amount') ?></th>
            <td><?= $this->Number->currency($purchaseOrder->total_amount, 'EUR') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery Moment') ?></th>
            <td><?= h($purchaseOrder->delivery_moment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($purchaseOrder->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($purchaseOrder->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Delivery') ?></th>
            <td><?= $purchaseOrder->delivery ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Closed') ?></th>
            <td><?= $purchaseOrder->closed ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Gerelateerde bestelregels') ?></h4>
        <?php if (!empty($purchaseOrder->suppliers_products_purchase_orders)): ?>
        <table>
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User') ?></th>
                <th scope="col"><?= __('Product') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Total') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($purchaseOrder->suppliers_products_purchase_orders as $suppliersProductsPurchaseOrder): ?>
            <tr>
                <td><?= h($suppliersProductsPurchaseOrder->id) ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->user->first_name .' '. $suppliersProductsPurchaseOrder->user->last_name) ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->suppliers_product->product->name) ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->quantity) ?></td>
                <td><?= $this->Number->currency($suppliersProductsPurchaseOrder->price, 'EUR') ?></td>
                <td><?= $this->Number->currency($suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price, 'EUR') ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->created) ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'view', $suppliersProductsPurchaseOrder->id]) ?>    
                    <?php if(!$purchaseOrder->closed): ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'edit', $suppliersProductsPurchaseOrder->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'delete', $suppliersProductsPurchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliersProductsPurchaseOrder->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
