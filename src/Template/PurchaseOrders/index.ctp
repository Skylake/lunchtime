<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PurchaseOrder[]|\Cake\Collection\CollectionInterface $purchaseOrders
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="purchaseOrders index large-10 medium-9 columns content">
    <h3><?= __('Purchase Orders') ?></h3>
    <table>
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_moment') ?></th>
                <th scope="col"><?= $this->Paginator->sort('delivery_cost') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comment') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('closed') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($purchaseOrders as $purchaseOrder): ?>
            <tr>
                <td><?= $this->Number->format($purchaseOrder->id) ?></td>
                <td><?= $purchaseOrder->has('supplier') ? $this->Html->link($purchaseOrder->supplier->name, ['controller' => 'Suppliers', 'action' => 'view', $purchaseOrder->supplier->id]) : '' ?></td>
                <td><?= $purchaseOrder->has('user') ? $this->Html->link($purchaseOrder->user->first_name .' '. $purchaseOrder->user->last_name, ['controller' => 'Users', 'action' => 'view', $purchaseOrder->user->id]) : '' ?></td>
                <td><?= $purchaseOrder->delivery ? __('Yes') : __('No'); ?></td>
                <td><?= h($purchaseOrder->delivery_moment) ?></td>
                <td><?= $this->Number->currency($purchaseOrder->delivery_cost, 'EUR') ?></td>
                <td><?= $this->Number->currency($purchaseOrder->total_amount, 'EUR') ?></td>
                <td><?= h($purchaseOrder->comment) ?></td>
                <td><?= h($purchaseOrder->created) ?></td>
                <td><?= $purchaseOrder->closed ? __('Yes') : __('No'); ?></td>
                <td class="actions">
                    <?php if ($purchaseOrder->closed): ?> 
                        <?= $this->Html->link(__('View'), ['action' => 'view', $purchaseOrder->id]) ?>
                        <?= $this->Html->link(__('Send'), ['action' => 'send', $purchaseOrder->id]) ?>
                    <?php else: ?>
                        <?= $this->Form->postLink(__('Close'), ['action' => 'close', $purchaseOrder->id], ['confirm' => __('Are you sure you want to close # {0}?', $purchaseOrder->id)]) ?>
                        <?= $this->Html->link(__('Add'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'add', $purchaseOrder->id]) ?>
                        <?= $this->Html->link(__('View'), ['action' => 'view', $purchaseOrder->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $purchaseOrder->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $purchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrder->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
