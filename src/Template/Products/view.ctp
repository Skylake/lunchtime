<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/product-menu') ?>
</nav>
<div class="products view large-10 medium-9 columns content">
    <h3><?= h($product->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($product->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($product->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($product->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($product->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Suppliers') ?></h4>
        <?php if (!empty($product->suppliers)): ?>
        <table>
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Website') ?></th>
                <th scope="col"><?= __('Phone') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Postalcode') ?></th>
                <th scope="col"><?= __('City') ?></th>
                <th scope="col"><?= __('Delivery') ?></th>
                <th scope="col"><?= __('Delivery Cost') ?></th>
                <th scope="col"><?= __('Delivery Cost Until') ?></th>
                <th scope="col"><?= __('Delivery Cost Minimum') ?></th>
                <th scope="col"><?= __('Attachment') ?></th>
                <th scope="col"><?= __('Active') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->suppliers as $suppliers): ?>
            <tr>
                <td><?= h($suppliers->id) ?></td>
                <td><?= h($suppliers->name) ?></td>
                <td><?= h($suppliers->website) ?></td>
                <td><?= h($suppliers->phone) ?></td>
                <td><?= h($suppliers->email) ?></td>
                <td><?= h($suppliers->address) ?></td>
                <td><?= h($suppliers->postalcode) ?></td>
                <td><?= h($suppliers->city) ?></td>
                <td><?= h($suppliers->delivery) ?></td>
                <td><?= h($suppliers->delivery_cost) ?></td>
                <td><?= h($suppliers->delivery_cost_until) ?></td>
                <td><?= h($suppliers->delivery_cost_minimum) ?></td>
                <td><?= h($suppliers->attachment) ?></td>
                <td><?= h($suppliers->active) ?></td>
                <td><?= h($suppliers->created) ?></td>
                <td><?= h($suppliers->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Suppliers', 'action' => 'view', $suppliers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Suppliers', 'action' => 'edit', $suppliers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Suppliers', 'action' => 'delete', $suppliers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
