<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * SuppliersProductsPurchaseOrders Model
 *
 * @property \App\Model\Table\SuppliersProductsTable&\Cake\ORM\Association\BelongsTo $SuppliersProducts
 * @property \App\Model\Table\PurchaseOrdersTable&\Cake\ORM\Association\BelongsTo $PurchaseOrders
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder get($primaryKey, $options = [])
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SuppliersProductsPurchaseOrder findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SuppliersProductsPurchaseOrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('suppliers_products_purchase_orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SuppliersProducts', [
            'foreignKey' => 'suppliers_product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PurchaseOrders', [
            'foreignKey' => 'purchase_order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('quantity')
            ->notEmptyString('quantity');

        $validator
            ->decimal('price')
            ->notEmptyString('price');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->allowEmptyString('comment');

        $validator
            ->boolean('paid')
            ->notEmptyString('paid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['suppliers_product_id'], 'SuppliersProducts'));
        $rules->add($rules->existsIn(['purchase_order_id'], 'PurchaseOrders'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * afterSave and afterDelete events to update the linked PurchaseOrder.
     */
    public function afterSave(\Cake\Event\Event $event, $entity, $options)
    {
        // recalculate total order amount.
        $purchaseOrder = TableRegistry::getTableLocator()->get('PurchaseOrders')->get($entity->purchase_order_id);
        $purchaseOrder->setDirty('total_amount', true);
        TableRegistry::getTableLocator()->get('PurchaseOrders')->save($purchaseOrder);
    }

    public function afterDelete(\Cake\Event\Event $event, $entity, $options)
    {
        // recalculate total order amount.
        $purchaseOrder = TableRegistry::getTableLocator()->get('PurchaseOrders')->get($entity->purchase_order_id);
        $purchaseOrder->setDirty('total_amount', true);
        TableRegistry::getTableLocator()->get('PurchaseOrders')->save($purchaseOrder);
    }
}
