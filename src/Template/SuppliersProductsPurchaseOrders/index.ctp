<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProductsPurchaseOrder[]|\Cake\Collection\CollectionInterface $suppliersProductsPurchaseOrders
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="suppliersProductsPurchaseOrders index large-10 medium-9 columns content">
    <h3><?= __('Suppliers Products Purchase Orders') ?></h3>
    <table>
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('suppliers_product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('purchase_order_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comment') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($suppliersProductsPurchaseOrders as $suppliersProductsPurchaseOrder): ?>
            <tr>
                <td><?= $this->Number->format($suppliersProductsPurchaseOrder->id) ?></td>
                <td><?= $suppliersProductsPurchaseOrder->has('suppliers_product') ? $this->Html->link($suppliersProductsPurchaseOrder->suppliers_product->product->name, ['controller' => 'SuppliersProducts', 'action' => 'view', $suppliersProductsPurchaseOrder->suppliers_product->id]) : '' ?></td>
                <td><?= $suppliersProductsPurchaseOrder->has('purchase_order') ? $this->Html->link($suppliersProductsPurchaseOrder->purchase_order->supplier->name, ['controller' => 'PurchaseOrders', 'action' => 'view', $suppliersProductsPurchaseOrder->purchase_order->id]) : '' ?></td>
                <td><?= $suppliersProductsPurchaseOrder->has('user') ? $this->Html->link($suppliersProductsPurchaseOrder->user->first_name .' '. $suppliersProductsPurchaseOrder->user->last_name, ['controller' => 'Users', 'action' => 'view', $suppliersProductsPurchaseOrder->user->id]) : '' ?></td>
                <td><?= $this->Number->format($suppliersProductsPurchaseOrder->quantity) ?></td>
                <td><?= $this->Number->currency($suppliersProductsPurchaseOrder->price, 'EUR') ?></td>
                <td><?= $this->Number->currency($suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price, 'EUR') ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->comment) ?></td>
                <td><?= $suppliersProductsPurchaseOrder->paid ? __('Yes') : __('No'); ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->created) ?></td>
                <td><?= h($suppliersProductsPurchaseOrder->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $suppliersProductsPurchaseOrder->id]) ?>
                    <?php if(!$suppliersProductsPurchaseOrder->paid): ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $suppliersProductsPurchaseOrder->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $suppliersProductsPurchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliersProductsPurchaseOrder->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
