<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProductsPurchaseOrder $suppliersProductsPurchaseOrder
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="suppliersProductsPurchaseOrders view large-10 medium-9 columns content">
    <h4><?= __('Open Purchase Orders') ?></h4>
    <?php if (!empty($openOrderPayments)): ?>
    <table>
        <?php foreach ($openOrderPayments as $purchaseOrders): ?>
        <tr>
            <th scope="col"><?= __('Order No.') ?></th>
            <th scope="col"><?= __('Supplier Name') ?></th>
            <th scope="col"><?= __('Purchaser') ?></th>
            <th scope="col"><?= __('Delivery Moment') ?></th>
            <th scope="col"><?= __('Total Amount') ?></th>
            <th scope="col"><?= __('Comment') ?></th>
            <th scope="col"><?= __('Closed') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        <tr>
            <td><?= h($purchaseOrders->id) ?></td>
            <td><?= h($purchaseOrders->supplier->name) ?></td>
            <td><?= h($purchaseOrders->user->first_name.' '.$purchaseOrders->user->last_name) ?></td>
            <td><?= h($purchaseOrders->delivery_moment) ?></td>
            <td><?= $this->Number->currency($purchaseOrders->total_amount, 'EUR') ?></td>
            <td><?= h($purchaseOrders->comment) ?></td>
            <td><?= $purchaseOrders->closed ? __('Yes') : __('No'); ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrders->id]) ?>
            </td>
        </tr>

        <?php if (!empty($purchaseOrders->suppliers_products_purchase_orders)): ?>
            <tr>
                <td colspan="8">
                <table>
                    <tr>
                        <th scope="col"><?= __('Ordered By') ?></th>
                        <th scope="col"><?= __('Product') ?></th>
                        <th scope="col"><?= __('Quantity') ?></th>
                        <th scope="col"><?= __('Price') ?></th>
                        <th scope="col"><?= __('Total Amount') ?></th>
                        <th scope="col"><?= __('Comment') ?></th>
                        <th scope="col"><?= __('Paid') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($purchaseOrders->suppliers_products_purchase_orders as $suppliersProductsPurchaseOrder): ?>
                    <tr>
                        <td><?= h($suppliersProductsPurchaseOrder->user->first_name.' '.$suppliersProductsPurchaseOrder->user->last_name) ?></td>
                        <td><?= h($suppliersProductsPurchaseOrder->suppliers_product->product->name) ?></td>
                        <td><?= h($suppliersProductsPurchaseOrder->quantity) ?></td>
                        <td><?= $this->Number->currency($suppliersProductsPurchaseOrder->price, 'EUR') ?></td>
                        <td><?= $this->Number->currency($suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price, 'EUR') ?></td>
                        <td><?= h($suppliersProductsPurchaseOrder->comment) ?></td>
                        <td><?= $suppliersProductsPurchaseOrder->paid ? __('Yes') : __('No'); ?></td>
                        <td class="actions">
                            <?php if(!$suppliersProductsPurchaseOrder->paid): ?>
                                <?= $this->Html->link(__('Pay'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'pay', $suppliersProductsPurchaseOrder->id]) ?>
                            <?php endif; ?>
                            <?= $this->Html->link(__('View'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'view', $suppliersProductsPurchaseOrder->id]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                </td>
            </tr>
        <?php endif; ?>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>
