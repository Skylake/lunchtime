<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuppliersProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuppliersProductsTable Test Case
 */
class SuppliersProductsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SuppliersProductsTable
     */
    public $SuppliersProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SuppliersProducts',
        'app.Suppliers',
        'app.Products',
        'app.PurchaseOrders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SuppliersProducts') ? [] : ['className' => SuppliersProductsTable::class];
        $this->SuppliersProducts = TableRegistry::getTableLocator()->get('SuppliersProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SuppliersProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
