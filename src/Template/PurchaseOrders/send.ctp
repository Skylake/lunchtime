<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PurchaseOrder $purchaseOrder
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="purchaseOrders view large-10 medium-9 columns content">
    <?= $this->Form->create($purchaseOrder->supplier) ?>
    <fieldset>
        <legend><?= __('Send Purchase Order') ?></legend>
        <?php
            echo $this->Form->control('email', ['required' => true, 'label' => 'E-mail recipient']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Send')) ?>
    <?= $this->Form->end() ?>
</div>