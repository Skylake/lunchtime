<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProductsPurchaseOrder $suppliersProductsPurchaseOrder
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="suppliersProductsPurchaseOrders view large-10 medium-9 columns content">
    <h3><?= h($suppliersProductsPurchaseOrder->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Suppliers Product') ?></th>
            <td><?= $suppliersProductsPurchaseOrder->has('suppliers_product') ? $this->Html->link($suppliersProductsPurchaseOrder->suppliers_product->product->name, ['controller' => 'SuppliersProducts', 'action' => 'view', $suppliersProductsPurchaseOrder->suppliers_product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Purchase Order') ?></th>
            <td><?= $suppliersProductsPurchaseOrder->has('purchase_order') ? $this->Html->link($suppliersProductsPurchaseOrder->purchase_order->supplier->name, ['controller' => 'PurchaseOrders', 'action' => 'view', $suppliersProductsPurchaseOrder->purchase_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $suppliersProductsPurchaseOrder->has('user') ? $this->Html->link($suppliersProductsPurchaseOrder->user->first_name .' '. $suppliersProductsPurchaseOrder->user->last_name, ['controller' => 'Users', 'action' => 'view', $suppliersProductsPurchaseOrder->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comment') ?></th>
            <td><?= h($suppliersProductsPurchaseOrder->comment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($suppliersProductsPurchaseOrder->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($suppliersProductsPurchaseOrder->quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->currency($suppliersProductsPurchaseOrder->price, 'EUR') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($suppliersProductsPurchaseOrder->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($suppliersProductsPurchaseOrder->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Paid') ?></th>
            <td><?= $suppliersProductsPurchaseOrder->paid ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
