<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuppliersProductsPurchaseOrdersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuppliersProductsPurchaseOrdersTable Test Case
 */
class SuppliersProductsPurchaseOrdersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SuppliersProductsPurchaseOrdersTable
     */
    public $SuppliersProductsPurchaseOrders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SuppliersProductsPurchaseOrders',
        'app.SuppliersProducts',
        'app.PurchaseOrders',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SuppliersProductsPurchaseOrders') ? [] : ['className' => SuppliersProductsPurchaseOrdersTable::class];
        $this->SuppliersProductsPurchaseOrders = TableRegistry::getTableLocator()->get('SuppliersProductsPurchaseOrders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SuppliersProductsPurchaseOrders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
