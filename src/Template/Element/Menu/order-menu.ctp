<ul class="side-nav">
    <li class="heading"><?= __('Bestellingen') ?></li>
    <li><?= $this->Html->link(__('Overzicht'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
    <li><?= $this->Html->link(__('Openstaand'), ['controller' => 'PurchaseOrders', 'action' => 'open']) ?></li>
    <li><?= $this->Html->link(__('Toevoegen'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
    <?php if (isset($purchaseOrder->id) && !$purchaseOrder->closed): ?>
        <li><?= $this->Html->link(__('Aanpassen'), ['controller' => 'PurchaseOrders', 'action' => 'edit', $purchaseOrder->id]) ?></li>
        <li><?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'PurchaseOrders', 'action' => 'delete', $purchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrder->id)]) ?></li>
    <?php endif; ?>
    <?php if (isset($purchaseOrder->id) && $purchaseOrder->closed): ?>
        <li><?= $this->Html->link(__('Versturen'), ['controller' => 'PurchaseOrders', 'action' => 'send', $purchaseOrder->id]) ?></li>
    <?php endif; ?>
    <li class="heading"><?= __('Bestelregel') ?></li>
    <li><?= $this->Html->link(__('Overzicht'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'index']) ?></li>
    <?php if (isset($purchaseOrder->id) && !$purchaseOrder->closed): ?>
        <li><?= $this->Html->link(__('Toevoegen'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'add', $purchaseOrder->id]) ?></li>
    <?php endif; ?>
    <?php if (isset($suppliersProductsPurchaseOrder->id) && !$suppliersProductsPurchaseOrder->paid): ?>
        <li><?= $this->Html->link(__('Aanpassen'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'edit', $suppliersProductsPurchaseOrder->id]) ?></li>
        <li><?= $this->Form->postLink(__('Verwijderen'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'delete', $suppliersProductsPurchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliersProductsPurchaseOrder->id)]) ?></li>
    <?php endif; ?>
    <li class="heading"><?= __('Betalingen') ?></li>
    <li><?= $this->Html->link(__('Openstaand'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'open']) ?></li>
</ul>