<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProductsPurchaseOrder $suppliersProductsPurchaseOrder
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="suppliersProductsPurchaseOrders form large-10 medium-9 columns content">
    <?= $this->Form->create($suppliersProductsPurchaseOrder) ?>
    <fieldset>
        <legend><?= __('Bestelregel betalen de voor bestelling van '.$suppliersProductsPurchaseOrder->purchase_order->delivery_moment.' bij '.$suppliersProductsPurchaseOrder->purchase_order->supplier->name) ?></legend>
        <?php
            echo $this->Form->control('payment_method', ['options' => $userPaymentMethods]);
            echo $this->Form->label(sprintf('Total cost: %s', $this->Number->currency($suppliersProductsPurchaseOrder->quantity * $suppliersProductsPurchaseOrder->price, 'EUR')));
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
