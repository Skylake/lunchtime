<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/gebruiker-menu') ?>
</nav>
<div class="users view large-10 medium-9 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($user->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($user->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($user->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Iban') ?></th>
            <td><?= h($user->iban) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Balance') ?></th>
            <td><?= $this->Number->currency($user->balance, 'EUR') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $user->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Purchase Orders') ?></h4>
        <?php if (!empty($user->purchase_orders)): ?>
        <table>
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Supplier Name') ?></th>
                <th scope="col"><?= __('User Name') ?></th>
                <th scope="col"><?= __('Delivery') ?></th>
                <th scope="col"><?= __('Delivery Moment') ?></th>
                <th scope="col"><?= __('Delivery Cost') ?></th>
                <th scope="col"><?= __('Total Amount') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Closed') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->purchase_orders as $purchaseOrders): ?>
            <tr>
                <td><?= h($purchaseOrders->id) ?></td>
                <td><?= h($purchaseOrders->supplier->name) ?></td>
                <td><?= h($purchaseOrders->user->first_name.' '.$purchaseOrders->user->last_name) ?></td>
                <td><?= $purchaseOrders->delivery ? __('Yes') : __('No') ?></td>
                <td><?= h($purchaseOrders->delivery_moment) ?></td>
                <td><?= $this->Number->currency($purchaseOrders->delivery_cost, 'EUR') ?></td>
                <td><?= $this->Number->currency($purchaseOrders->total_amount, 'EUR') ?></td>
                <td><?= h($purchaseOrders->comment) ?></td>
                <td><?= $purchaseOrders->closed ? __('Yes') : __('No') ?></td>
                <td><?= h($purchaseOrders->created) ?></td>
                <td><?= h($purchaseOrders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrders->id]) ?>
                    <?php if (!$purchaseOrders->closed): ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'PurchaseOrders', 'action' => 'edit', $purchaseOrders->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'PurchaseOrders', 'action' => 'delete', $purchaseOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrders->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Suppliers Products Purchase Orders') ?></h4>
        <?php if (!empty($user->suppliers_products_purchase_orders)): ?>
        <table>
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Product Name') ?></th>
                <th scope="col"><?= __('Order No.') ?></th>
                <th scope="col"><?= __('User Name') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Paid') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->suppliers_products_purchase_orders as $suppliersProductsPurchaseOrders): ?>
            <tr>
                <td><?= h($suppliersProductsPurchaseOrders->id) ?></td>
                <td><?= h($suppliersProductsPurchaseOrders->suppliers_product->product->name) ?></td>
                <td><?= h($suppliersProductsPurchaseOrders->purchase_order_id) ?></td>
                <td><?= h($suppliersProductsPurchaseOrders->user->first_name.' '.$suppliersProductsPurchaseOrders->user->last_name) ?></td>
                <td><?= h($suppliersProductsPurchaseOrders->quantity) ?></td>
                <td><?= $this->Number->currency($suppliersProductsPurchaseOrders->price, 'EUR') ?></td>
                <td><?= h($suppliersProductsPurchaseOrders->comment) ?></td>
                <td><?= $suppliersProductsPurchaseOrders->paid ? __('Yes') : __('No') ?></td>
                <td><?= h($suppliersProductsPurchaseOrders->created) ?></td>
                <td><?= h($suppliersProductsPurchaseOrders->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'view', $suppliersProductsPurchaseOrders->id]) ?>
                    <?php if (!$suppliersProductsPurchaseOrders->paid): ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'edit', $suppliersProductsPurchaseOrders->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'SuppliersProductsPurchaseOrders', 'action' => 'delete', $suppliersProductsPurchaseOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliersProductsPurchaseOrders->id)]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
