<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuppliersProductsPurchaseOrder $suppliersProductsPurchaseOrder
 */
?>
<nav class="large-2 medium-3 columns" id="actions-sidebar">
    <?= $this->element('Menu/order-menu') ?>
</nav>
<div class="suppliersProductsPurchaseOrders form large-10 medium-9 columns content">
    <?= $this->Form->create($suppliersProductsPurchaseOrder) ?>
    <fieldset>
        <legend><?= __('Bestelregel toevoegen aan bestelling '.$purchaseOrder->id.' ('.$purchaseOrder->supplier->name.')') ?></legend>
        <?php
            echo $this->Form->control('suppliers_product_id', ['options' => $suppliersProducts]);
            echo $this->Form->control('quantity');
            echo $this->Form->control('comment');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
